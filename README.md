# Routee SMS notification with Weather updates

## Installation

```bash
docker-compose up -d
docker-compose exec php php composer.phar install
```

## Usage

Just invoke script in PHP container:

```bash
docker-compose exec php php index.php
```
