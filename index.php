<?php

require_once 'vendor/autoload.php';

$container = new \App\Containers\ApplicationContainer();
$container->registerProvider(new \App\Providers\ApplicationProvider);

$application = new \App\Application($container);

$application->run();
