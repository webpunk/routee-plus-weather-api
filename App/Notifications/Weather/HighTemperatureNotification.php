<?php

namespace App\Notifications\Weather;

class HighTemperatureNotification extends BaseWeatherNotification
{
    function toSmsText(): string
    {
        return "Hi, $this->name! Temperature is more than 20C. $this->actualTemperature";
    }
}
