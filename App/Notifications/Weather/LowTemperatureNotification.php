<?php

namespace App\Notifications\Weather;

class LowTemperatureNotification extends BaseWeatherNotification
{
    function toSmsText(): string
    {
        return "Hi, $this->name! Temperature is less than 20C. $this->actualTemperature";
    }
}
