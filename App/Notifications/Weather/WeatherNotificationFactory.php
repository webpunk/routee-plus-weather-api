<?php

namespace App\Notifications\Weather;

use App\Contracts\Notifications\Weather\WeatherNotificationInterface;

class WeatherNotificationFactory
{
    const BREAKING_TEMPERATURE = 20.0;

    public static function getNotificationForTemperature(float $actualTemperature): WeatherNotificationInterface
    {
        if ($actualTemperature > self::BREAKING_TEMPERATURE) {
            return new HighTemperatureNotification($actualTemperature);
        }

        return new LowTemperatureNotification($actualTemperature);

    }
}
