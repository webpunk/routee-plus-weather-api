<?php

namespace App\Notifications\Weather;

use App\Contracts\Notifications\NotificationChannelInterface;
use App\Contracts\Notifications\Weather\WeatherNotificationInterface;
use App\Notifications\BaseNotification;

abstract class BaseWeatherNotification extends BaseNotification implements WeatherNotificationInterface
{
    /**
     * @var float
     */
    protected $actualTemperature;
    /**
     * @var string
     */
    protected $name;

    /**
     * @param float $actualTemperature
     */
    public function __construct(float $actualTemperature)
    {
        $this->actualTemperature = $actualTemperature;
    }

    public function setUserName(string $name): void
    {
        $this->name = $name;
    }
}
