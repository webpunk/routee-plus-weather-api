<?php

namespace App\Notifications\Drivers\SMS\Factories;

use App\Contracts\Notifications\Drivers\DriverFactoryInterface;
use App\Contracts\Notifications\Drivers\DriverInterface;
use App\Contracts\Notifications\Drivers\SMSDriverInterface;
use App\Contracts\Notifications\NotificationChannelInterface;
use App\Enums\NotificationChannelTypeEnum;
use App\Exceptions\Notifications\DriverNotDefinedException;

class RouteeDriverFactory implements DriverFactoryInterface
{
    /**
     * @var array
     */
    private $drivers;

    public function __construct(
        SMSDriverInterface $SMSDriver
    )
    {
        $this->setDriver(NotificationChannelTypeEnum::SMS, $SMSDriver);
    }

    /**
     * @throws DriverNotDefinedException
     */
    public function getDriverFor(NotificationChannelInterface $channel): DriverInterface
    {
        if (!isset($this->drivers[$channel->getType()])) {
            throw new DriverNotDefinedException;
        }

        return $this->drivers[$channel->getType()];
    }

    protected function setDriver(string $channelType, DriverInterface $driver) {
        $this->drivers[$channelType] = $driver;
    }
}
