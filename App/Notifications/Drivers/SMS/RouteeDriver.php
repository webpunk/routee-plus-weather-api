<?php

namespace App\Notifications\Drivers\SMS;

use App\Contracts\Notifications\Drivers\SMSDriverInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class RouteeDriver implements SMSDriverInterface
{
    const API_BASE_URL = "https://connect.routee.net/";
    const API_AUTH_URL = "https://auth.routee.net/oauth/token";

    const API_APPLICATION_ID = "5c5d5e28e4b0bae5f4accfec"; // @TODO: Create config service to load keys from environment variable
    const API_APPLICATION_SECRET = "MGkNfqGud0"; // @TODO: Create config service to load keys from environment variable

    /**
     * @var string
     */
    protected $authToken;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @throws GuzzleException
     */
    public function __construct()
    {
        $this->client = new Client;
        $this->authorize();
    }

    public function send(string $route, array $payload)
    {
        $this->makeApiCall('sms', [
            "body" => $payload["text"],
            "to" => $route,
            "from" => "amdTelecom",
        ], 'POST');
    }

    /**
     * @throws GuzzleException
     */
    protected function authorize(): void {
        $response = $this->client->post(self::API_AUTH_URL, [
            "headers" => [
                "Authorization" => "Basic " . $this->getBasicAuthToken(),
            ],
            "form_params" => [
                "grant_type" => "client_credentials"
            ],
        ]);

        $this->authToken = json_decode($response->getBody()->getContents())->access_token;
    }

    protected function makeApiCall(string $path, array $params = [], string $method = 'GET') {
        $response = $this->client->request($method, self::API_BASE_URL . $path, [
            "headers" => [
                "Authorization" => "Bearer " . $this->getBearerAuthToken(),
                "Content-type" => "	application/json",
            ],
            "json" => $params,
        ]);

        return json_decode($response->getBody()->getContents());
    }

    protected function getBasicAuthToken(): string {
        return base64_encode(self::API_APPLICATION_ID.":".self::API_APPLICATION_SECRET);
    }
    protected function getBearerAuthToken(): string {
        return $this->authToken;
    }
}
