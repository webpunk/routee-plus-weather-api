<?php

namespace App\Notifications\Channels;

use App\Contracts\Notifications\NotificationChannelInterface;
use App\Enums\NotificationChannelTypeEnum;

class SMSChannel implements NotificationChannelInterface
{
    public function getType(): string
    {
        return NotificationChannelTypeEnum::SMS;
    }
}
