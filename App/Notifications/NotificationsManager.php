<?php

namespace App\Notifications;

use App\Contracts\Notifications\Drivers\DriverFactoryInterface;
use App\Contracts\Notifications\Notifiable;
use App\Contracts\Notifications\NotificationChannelInterface;
use App\Contracts\Notifications\NotificationInterface;
use App\Contracts\Notifications\NotificationsManagerInterface;

class NotificationsManager implements NotificationsManagerInterface
{
    /**
     * @var DriverFactoryInterface
     */
    protected $driverFactory;

    public function __construct(DriverFactoryInterface $driverFactory)
    {
        $this->driverFactory = $driverFactory;
    }

    public function sendNotification(NotificationInterface $notification, Notifiable $notifiable): void
    {
        $channels = $notification->viaChannels();

        foreach ($channels as $channel) {
            /** @var NotificationChannelInterface $channel */
            $channel = new $channel;
            $driver = $this->driverFactory->getDriverFor($channel);
            $driver->send(
                $notifiable->getNotificationChannelRoute($channel),
                $notification->getPayloadForChannel($channel)
            );
        }
    }
}
