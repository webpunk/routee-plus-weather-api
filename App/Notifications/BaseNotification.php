<?php

namespace App\Notifications;

use App\Contracts\Notifications\NotificationChannelInterface;
use App\Contracts\Notifications\NotificationInterface;
use App\Enums\NotificationChannelTypeEnum;
use App\Notifications\Channels\SMSChannel;

abstract class BaseNotification
{
    /**
     * @return string[]
     */
    public function viaChannels(): array
    {
        return [SMSChannel::class];
    }

    public function getPayloadForChannel(NotificationChannelInterface $notificationChannel): array
    {
        switch ($notificationChannel->getType()) {
            case NotificationChannelTypeEnum::SMS:
            default:
                return [
                    "text" => $this->toSmsText()
                ];
        }
    }

    abstract function toSmsText(): string;
}
