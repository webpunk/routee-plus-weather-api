<?php

namespace App\Entities;

use App\Contracts\Notifications\Notifiable;
use App\Contracts\Notifications\NotificationChannelInterface;
use App\Enums\NotificationChannelTypeEnum;

class User implements Notifiable
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $phoneNumber;

    public function __construct(array $data)
    {
        $this->name = $data["name"];
        $this->phoneNumber = $data["phoneNumber"];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function getNotificationChannelRoute(NotificationChannelInterface $channel): string
    {
        switch ($channel->getType()) {
            case NotificationChannelTypeEnum::SMS:
            default:
                return $this->phoneNumber;
        }
    }
}
