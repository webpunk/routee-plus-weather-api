<?php

namespace App\Repositories;

use App\Contracts\Repositories\UsersRepositoryInterface;
use App\Entities\User;

class MockUsersRepository implements UsersRepositoryInterface
{
    protected $users = [
        [
            "name" => "Alex",
            "phoneNumber" => "+306911111111"
        ]
    ];

    public function getAll(): array
    {
        return array_map(function (array $userData) {
            return new User($userData);
        }, $this->users);
    }
}
