<?php

namespace App\Containers;

use App\Contracts\ApplicationContainerInterface;
use App\Contracts\ProviderInterface;
use App\Exceptions\DependencyNotFoundException;
use App\Exceptions\DependencyNotResolvableException;
use ReflectionClass;
use ReflectionException;

class ApplicationContainer implements ApplicationContainerInterface
{
    protected $container = [];

    public function get(string $id)
    {
        if (!$this->has($id)) {
            throw new DependencyNotFoundException;
        }

        return $this->resolve($this->container[$id]);
    }

    public function has(string $id): bool
    {
        return isset($this->container[$id]);
    }

    public function bind(string $abstract, $concrete): void
    {
        $this->container[$abstract] = $concrete;
    }

    /**
     * @throws ReflectionException
     * @throws DependencyNotResolvableException
     */
    public function resolve(string $class)
    {
        $reflector = new ReflectionClass($class);
        $constructor = $reflector->getConstructor();
        $constructorParameters = $constructor ? $constructor->getParameters() : [];

        if (!$constructor || !$constructorParameters) {
            return $reflector->newInstance();
        }

        $resolved = [];
        foreach ($constructorParameters as $parameter) {
            if ($parameter->getClass()) {
                $resolved []= $this->get($parameter->getType());
                continue;
            }

            if ($defaultValue = $parameter->getDefaultValue()) {
                $resolved []= $defaultValue;
                continue;
            }

            throw new DependencyNotResolvableException;
        }

        return $reflector->newInstance(...$resolved);
    }

    public function registerProvider(ProviderInterface $provider): void
    {
        $provider->setContainer($this);
        $provider->register();
    }
}
