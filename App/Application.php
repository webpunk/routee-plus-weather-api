<?php

namespace App;

use App\Contracts\ApplicationContainerInterface;
use App\Contracts\ApplicationInterface;
use App\Contracts\JobInterface;
use App\Jobs\WeatherReportJob;

class Application implements ApplicationInterface
{
    /**
     * @var ApplicationContainerInterface
     */
    private $container;

    public function __construct(ApplicationContainerInterface $container)
    {
        $this->container = $container;
    }

    public function run(): void
    {
        /** @var JobInterface $job */
        $job = $this->container->resolve(WeatherReportJob::class);

        $job->dispatch();
    }
}
