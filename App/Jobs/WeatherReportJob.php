<?php

namespace App\Jobs;

use App\Contracts\JobInterface;
use App\Contracts\Notifications\NotificationsManagerInterface;
use App\Contracts\Repositories\UsersRepositoryInterface;
use App\Contracts\Services\WeatherServiceInterface;
use App\Notifications\Weather\BaseWeatherNotification;
use App\Notifications\Weather\WeatherNotificationFactory;

class WeatherReportJob implements JobInterface
{
    /**
     * @var UsersRepositoryInterface
     */
    protected $usersRepository;

    /**
     * @var NotificationsManagerInterface
     */
    protected $notificationsManager;

    /**
     * @var WeatherServiceInterface
     */
    protected $weatherService;

    public function __construct(
        UsersRepositoryInterface $usersRepository,
        NotificationsManagerInterface $notificationsManager,
        WeatherServiceInterface $weatherService
    )
    {
        $this->usersRepository = $usersRepository;
        $this->notificationsManager = $notificationsManager;
        $this->weatherService = $weatherService;
    }

    public function dispatch(): void
    {
        $users = $this->usersRepository->getAll();

        $actualTemperature = $this->weatherService->getActualTemperature();

        $notification = WeatherNotificationFactory::getNotificationForTemperature($actualTemperature);

        foreach ($users as $user) {

            $notification->setUserName($user->getName());
            $this->notificationsManager->sendNotification($notification, $user);
        }
    }
}
