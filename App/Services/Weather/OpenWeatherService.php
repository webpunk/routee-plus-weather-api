<?php

namespace App\Services\Weather;

use App\Contracts\Services\WeatherServiceInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class OpenWeatherService implements WeatherServiceInterface
{
    const API_BASE_URL = "https://api.openweathermap.org/data/2.5/";
    const API_KEY = "b385aa7d4e568152288b3c9f5c2458a5"; // @TODO: Create config service to load keys from environment variable

    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            "base_uri" => self::API_BASE_URL,
        ]);
    }

    public function getActualTemperature(): float
    {
        /**
        * @TODO: Remove hardcoded City - move it to User entity
        **/
        $response = $this->makeApiCall('weather', [
            'q' => 'Thessaloniki',
        ]);

        return (float)$response->main->temp;
    }

    protected function makeApiCall(string $path, array $query = [], string $method = 'GET')
    {
        $response = $this->client->request($method, $path, [
            "query" => $query + [
                "appid" => self::API_KEY,
                "units" => 'metric',
            ]
        ]);

        return json_decode($response->getBody()->getContents());
    }
}
