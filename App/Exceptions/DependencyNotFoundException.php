<?php

namespace App\Exceptions;

use Psr\Container\NotFoundExceptionInterface;

class DependencyNotFoundException extends \Exception implements NotFoundExceptionInterface
{

}
