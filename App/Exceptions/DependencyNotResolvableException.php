<?php

namespace App\Exceptions;

use Psr\Container\NotFoundExceptionInterface;

class DependencyNotResolvableException extends \Exception implements NotFoundExceptionInterface
{

}
