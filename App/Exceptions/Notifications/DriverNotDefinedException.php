<?php

namespace App\Exceptions\Notifications;

use Throwable;

class DriverNotDefinedException extends \Exception
{
    public function __construct($code = 0, Throwable $previous = null)
    {
        $message = "Driver for this channel type not exists";
        parent::__construct($message, $code, $previous);
    }
}
