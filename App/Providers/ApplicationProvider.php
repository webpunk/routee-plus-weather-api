<?php

namespace App\Providers;

use App\Contracts\ApplicationContainerInterface;
use App\Contracts\Notifications\Drivers\DriverFactoryInterface;
use App\Contracts\Notifications\Drivers\SMSDriverInterface;
use App\Contracts\Notifications\NotificationsManagerInterface;
use App\Contracts\ProviderInterface;
use App\Contracts\Repositories\UsersRepositoryInterface;
use App\Contracts\Services\WeatherServiceInterface;
use App\Notifications\Drivers\SMS\RouteeDriver;
use App\Notifications\Drivers\SMS\Factories\RouteeDriverFactory;
use App\Notifications\NotificationsManager;
use App\Repositories\MockUsersRepository;
use App\Services\Weather\OpenWeatherService;

class ApplicationProvider implements ProviderInterface
{
    /**
     * @var ApplicationContainerInterface
     */
    protected $container;

    public function register(): void
    {
        $this->container->bind(UsersRepositoryInterface::class, MockUsersRepository::class);
        $this->container->bind(NotificationsManagerInterface::class, NotificationsManager::class);
        $this->container->bind(WeatherServiceInterface::class, OpenWeatherService::class);

        $this->container->bind(DriverFactoryInterface::class, RouteeDriverFactory::class);
        $this->container->bind(SMSDriverInterface::class, RouteeDriver::class);
    }

    public function setContainer(ApplicationContainerInterface $container): void
    {
        $this->container = $container;
    }
}
