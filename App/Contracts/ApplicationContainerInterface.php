<?php

namespace App\Contracts;

use Psr\Container\ContainerInterface;

interface ApplicationContainerInterface extends ContainerInterface
{
    public function bind(string $abstract, $concrete): void;

    public function registerProvider(ProviderInterface $provider): void;

    public function resolve(string $class);
}
