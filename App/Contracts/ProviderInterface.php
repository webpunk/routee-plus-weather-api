<?php

namespace App\Contracts;

interface ProviderInterface
{
    public function register(): void;

    public function setContainer(ApplicationContainerInterface $container): void;
}
