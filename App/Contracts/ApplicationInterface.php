<?php

namespace App\Contracts;

interface ApplicationInterface
{
    public function run(): void;
}
