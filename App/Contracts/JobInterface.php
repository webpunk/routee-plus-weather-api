<?php

namespace App\Contracts;

interface JobInterface
{
    public function dispatch(): void;
}
