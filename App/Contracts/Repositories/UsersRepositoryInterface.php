<?php

namespace App\Contracts\Repositories;

use App\Entities\User;

interface UsersRepositoryInterface
{
    /**
     * @return User[]
     */
    public function getAll(): array;
}
