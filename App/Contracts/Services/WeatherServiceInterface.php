<?php

namespace App\Contracts\Services;

interface WeatherServiceInterface
{
    public function getActualTemperature(): float;
}
