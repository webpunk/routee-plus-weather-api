<?php

namespace App\Contracts\Notifications;

interface NotificationsManagerInterface
{
    public function sendNotification(NotificationInterface $notification, Notifiable $notifiable): void;
}
