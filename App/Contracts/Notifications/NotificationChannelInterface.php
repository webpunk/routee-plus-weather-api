<?php

namespace App\Contracts\Notifications;

interface NotificationChannelInterface
{
    public function getType(): string;
}
