<?php

namespace App\Contracts\Notifications\Weather;

use App\Contracts\Notifications\NotificationInterface;

interface WeatherNotificationInterface extends NotificationInterface
{
    public function setUserName(string $name): void;
}
