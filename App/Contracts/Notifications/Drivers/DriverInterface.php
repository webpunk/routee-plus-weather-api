<?php

namespace App\Contracts\Notifications\Drivers;

interface DriverInterface
{
    public function send(string $route, array $payload);
}
