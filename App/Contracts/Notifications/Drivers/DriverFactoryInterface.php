<?php

namespace App\Contracts\Notifications\Drivers;

use App\Contracts\Notifications\NotificationChannelInterface;

interface DriverFactoryInterface
{
    public function getDriverFor(NotificationChannelInterface $channel): DriverInterface;
}
