<?php

namespace App\Contracts\Notifications;

interface NotificationInterface
{
    /**
     * @return NotificationChannelInterface[]
     */
    public function viaChannels(): array;

    public function getPayloadForChannel(NotificationChannelInterface $notificationChannel): array;
}
