<?php

namespace App\Contracts\Notifications;

interface Notifiable
{
    public function getNotificationChannelRoute(NotificationChannelInterface $channel): string;
}
